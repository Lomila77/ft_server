# Last small Debian
FROM    debian:buster-slim

# No question
ENV     DEBIAN_FRONTEND=noninteractive

# Install Backports (for php version)
RUN     echo "deb http://deb.debian.org/debian buster-backports main" \
	> /etc/apt/sources.list.d/backport.list \
# Update apt packages
	&& apt-get update --yes --quiet \
# Install and loading MariaDb-server
	&& apt-get install --yes --no-install-recommends --no-install-suggests \
	mariadb-server \
	&& service mysql start \
# Install others packages
	&& apt-get install --yes --no-install-recommends --no-install-suggests \
	nginx \
	php-fpm \
	php-twig/buster-backports \
	phpmyadmin/buster-backports \
	wordpress \
	systemctl \
	wordpress-theme-twentynineteen \
# Cleaning infos (of packages) to have more space
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
# Create and administrate MariaDB
	&& mysql -u root -e "CREATE DATABASE db_wp;" \
	&& mysql -u root -e "CREATE USER 'db_usrwp'@'localhost' IDENTIFIED BY 'db_pwwp';" \
	&& mysql -u root -e "GRANT ALL PRIVILEGES ON db_wp.* TO 'db_usrwp'@'localhost';" \
	&& mysql -u root -e "FLUSH PRIVILEGES;" \
# Editing with new MariaDB value wordpress file
	&& cp /usr/share/wordpress/wp-config-sample.php /etc/wordpress/wp-config.php \
	&& sed -i 's/database_name_here/db_wp/' /etc/wordpress/wp-config.php \
	&& sed -i 's/username_here/db_usrwp/' /etc/wordpress/wp-config.php \
	&& sed -i 's/password_here/db_pwwp/' /etc/wordpress/wp-config.php \
# Delete default nginx file
	&& rm /etc/nginx/sites-enabled/default \
# Link
	&& ln -s /etc/nginx/sites-available/wordpress.conf /etc/nginx/sites-enabled/wordpress.conf

# Openning port
EXPOSE  80 443

# Copy srcs to docker
COPY	srcs/nginx.conf /etc/nginx/sites-available/wordpress.conf
COPY	srcs/wp-config.php /etc/wordpress/config-wordpress.php
COPY	srcs/openssl_cert.cnf /etc/ssl/
COPY	srcs/ssl-params.conf /etc/nginx/snippets/

# Rename wordpress file with correct ip
CMD		mv /etc/wordpress/wp-config.php /etc/wordpress/config-$(ip a | grep 192 | xargs | cut -d " " - -f 2 | cut -d "/" -f 1).php \
# Create ssl certificate self-signed
	&& cat /etc/ssl/openssl_cert.cnf | openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt \
# Start service
	&& service nginx start \
	&& service mysql start \
	&& service php7.3-fpm start \
# Start Bash
	&& bash
