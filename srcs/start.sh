#!/usr/bin/env bash
set -euo pipefail
docker system prune -f
docker build -t ft_server .
docker run --network host --name=ft_server -p 8080:80 -p 4443:443 -it --rm ft_server
